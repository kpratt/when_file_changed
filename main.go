package main

import (
	"flag"
	"fmt"
	"github.com/howeyc/fsnotify"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
)

//var help      = flag.Bool("help", false, "Ignore all other arguments and print this help message")
var onModify = flag.Bool("m", false, "Trigger <cmd> on file modify")
var onCreate = flag.Bool("c", false, "Trigger <cmd> on file modify")
var onDelete = flag.Bool("d", false, "Trigger <cmd> on file modify")
var triggerOnce = flag.Bool("1", false, "Trigger the <cmd> on first event, then exit")
var grep = flag.String("regex", ".*", "Only trigger if file matches the regex")
var gitignore = flag.Bool("gitignore", true, "(Not implemented) Ignore all events on files not tracked by git.")

var regex *regexp.Regexp = nil

func actOnEvent(e *fsnotify.FileEvent) bool {
	eType := false
	if *onModify && e.IsModify() || e.IsRename() {
		eType = true
	} else if *onDelete && e.IsDelete() {
		eType = true
	} else if *onCreate && e.IsCreate() {
		eType = true
	}

	matched := regex.MatchString(e.Name)

	return matched && eType
}

func main() {
	flag.Parse()

	if len(flag.Args()) < 1 {
		fmt.Println("Error: no cmd was given")
		os.Exit(1)
	}

	args := flag.Args()
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}

	// If no overrides where given trigger on all modifications
	if !*onModify && !*onCreate && !*onDelete {
		*onModify = true
		*onCreate = true
		*onDelete = true
	}

	regex, err = regexp.Compile(*grep)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	cmdName := args[0]
	cmdArgs := strings.Join(args[1:], " ")

	fmt.Printf("\n")
	fmt.Printf("Triggering command\t [ %s %s ] on:\n", cmdName, cmdArgs)
	fmt.Printf("\tfile modified    [ %t ]\n", *onModify)
	fmt.Printf("\tfile created     [ %t ]\n", *onCreate)
	fmt.Printf("\tfile deleted     [ %t ]\n", *onDelete)
	fmt.Printf("\tIF file matches  [ %s ]\n", *grep)
	fmt.Printf("========================================\n")
	fmt.Printf("\n")

	done := make(chan bool)
	// Process events
	go func() {
		for {
			select {
			case ev := <-watcher.Event:
				if actOnEvent(ev) {
					log.Println("Event on file: ", ev.Name)
					cmd := exec.Command(cmdName, cmdArgs)
					output, _ := cmd.StdoutPipe()
					err = cmd.Start()
					if err == nil {
						bytes, _ := ioutil.ReadAll(output)
						exitError := cmd.Wait()
						if exitError == nil {
							log.Println("CMD succeeded.")
						} else {
							log.Fatal(exitError.Error())
						}
						fmt.Println(string(bytes))
						if *triggerOnce {
							done <- true
						}
					} else {
						log.Fatal(err)
					}
				}
			case err := <-watcher.Error:
				log.Println("error:", err)
			}
		}
	}()

	err = filepath.Walk(".", func(path string, f os.FileInfo, err error) error {
		permBits := int(f.Mode().Perm())
		if f.IsDir() && (permBits&0500) != 0 {
			err = watcher.Watch(path)
			if err != nil {
				log.Fatal(err)
				//				return err
			}
		}
		return nil

	})

	<-done

	watcher.Close()
}

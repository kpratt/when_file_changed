# Installation

[Install go](https://golang.org/doc/install) if you haven't already, then from the command line run:

`go get github.com/howeyc/fsnotify`

`go get bitbucket.org/kpratt/when_file_changed`


# Usage

When a file matching the given regex is modified run the specified command.

All events are caught by default. 
If an event flag is specified all unspecified event types are ignored.

Usage of when_file_changed:

+  -1	Trigger the <cmd> on first event, then exit
+  -c	Trigger <cmd> on file create
+  -d	Trigger <cmd> on file delete
+  -gitignore
    	(Not implemented) Ignore all events on files not tracked by git. (default true)
+  -m	Trigger <cmd> on file modified
+  -regex string
    	Only trigger if file matches the regex (default ".*")

### Ex.

`> when_file_changed -m -regex \\.go$ go build`

Will attempt to rebuild the project any time a go source file is modified :D

